package nl.bartpelle.luars;

/**
 * Created by Bart on 2-2-2015.
 *
 */
public interface LuaImplementation {


	public void animatePlayer(Object reference, int id);

	public void messagePlayer(Object reference, String message);

	public void openInterface(Object reference, int id, int target, int targetSub, boolean autoclose);

	public String playerName(Object reference);

}
