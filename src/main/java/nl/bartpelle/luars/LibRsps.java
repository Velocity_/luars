package nl.bartpelle.luars;

import org.luaj.vm2.LuaBoolean;
import org.luaj.vm2.LuaClosure;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.*;

/**
 * Created by Bart on 2/1/2015.
 *
 * Provides standard functionality commonly used in private servers.
 * Extended functionality can be provided by third-party libraries, but
 * note that that breaks the idea of a global scripting language which can
 * be plugged into whatever server.
 */
public class LibRsps extends TwoArgFunction {

	private LuaRS ctx;

	public LibRsps(LuaRS ctx) {
		this.ctx = ctx;
	}

	public LuaValue call(LuaValue modname, LuaValue env) {
		env.set("delay", new delay());

		/* Trigger functions */
		env.set("on_login", new on_login().ctx(ctx));
		env.set("on_button", new on_button().ctx(ctx));
		return NIL;
	}

	static class delay extends OneArgFunction {
		public LuaValue call(LuaValue list) {
			return NONE;
		}
	}

	static class on_login extends LuaRSFunction {
		public LuaValue call(LuaValue v) {
			ctx.register(ListenerType.ON_LOGIN, ((LuaClosure) v.checkfunction()).p);
			return LuaBoolean.TRUE;
		}
	}

	static class on_button extends LuaRSThreeArgFunction {
		public Varargs invoke(Varargs args) {
			if (args.isfunction(1) && args.isnumber(2) && args.isnumber(3) && args.isnumber(4))
				ctx.registerButton(ListenerType.ON_BUTTON, ((LuaClosure) args.checkfunction(1)).p, args.checkint(2), (args.checkint(3) << 16) | args.checkint(4));
			else if (args.isfunction(1) && args.isnumber(2) && args.isnumber(3))
				ctx.registerButton(ListenerType.ON_BUTTON, ((LuaClosure) args.checkfunction(1)).p, args.checkint(2), args.checkint(3));
			return LuaBoolean.TRUE;
		}
	}

	static abstract class LuaRSFunction extends OneArgFunction {
		protected LuaRS ctx;
		public LuaRSFunction ctx(LuaRS ctx) {
			this.ctx = ctx;
			return this;
		}
	}

	static abstract class LuaRSThreeArgFunction extends VarArgFunction {
		protected LuaRS ctx;
		public LuaRSThreeArgFunction ctx(LuaRS ctx) {
			this.ctx = ctx;
			return this;
		}
	}
}