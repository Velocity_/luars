package nl.bartpelle.luars;

import org.luaj.vm2.*;

/**
* Created by Bart on 2/1/2015.
*/
final class ManagedLuaClosure extends LuaClosure {

	private int i, a, b, c, pc = 0, top = 0;
	private LuaValue o;
	private Varargs v = NONE;
	private int[] code = p.code;
	private LuaValue[] k = p.k;
	private LuaValue[] stack;
	private LuaRS luars;
	public ParkReason parkReason;
	public int numwait;
	public ManagedLuaClosure nested, parent;

	// upvalues are only possible when closures create closures
	// TODO: use linked list.
	UpValue[] openups;

	/**
	 * Create a closure around a Prototype with a specific environment.
	 * If the prototype has upvalues, the environment will be written into the first upvalue.
	 *
	 * @param p   the Prototype to construct this Closure for.
	 * @param env the environment to associate with the closure.
	 */
	public ManagedLuaClosure(LuaRS luars, Prototype p, LuaValue env) {
		super(p, env);

		stack = new LuaValue[p.maxstacksize];
		for (int i = 0; i < p.numparams; ++i )
			stack[i] = NIL;
		openups = p.p.length > 0 ? new UpValue[stack.length] : null;

		this.luars = luars;
	}

	private int getUpvalIndex(UpValue v) {
		try {
			return (Integer) v.getClass().getDeclaredField("index").get(v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private UpValue findupval(LuaValue[] stack, short idx, UpValue[] openups) {
		final int n = openups.length;
		for (int i = 0; i < n; ++i)
			if (openups[i] != null && getUpvalIndex(openups[i]) == idx)
				return openups[i];
		for (int i = 0; i < n; ++i)
			if (openups[i] == null)
				return openups[i] = new UpValue(stack, idx);
		error("No space for upvalue");
		return null;
	}

	public void cont() {
		execute(new LuaValue[]{LuaValue.NONE}, LuaValue.varargsOf(new LuaValue[0]));
	}

	public void run(LuaValue... args) {
		System.arraycopy(args, 0, stack, 0, args.length);
		execute(stack, null);
	}

	public Varargs execute(LuaValue[] _stack, Varargs varargs) {
		if (nested != null) {
			nested.execute(_stack, varargs);
			return NONE;
		}
		// loop through instructions


		// process instructions
		try {
			for (; true; ++pc) {
				// pull out instruction
				i = code[pc];
				a = ((i >> 6) & 0xff);

				// process the op code
				switch (i & 0x3f) {

					case Lua.OP_MOVE:/*	A B	R(A):= R(B)					*/
						stack[a] = stack[i >>> 23];
						continue;

					case Lua.OP_LOADK:/*	A Bx	R(A):= Kst(Bx)					*/
						stack[a] = k[i >>> 14];
						continue;

					case Lua.OP_LOADBOOL:/*	A B C	R(A):= (Bool)B: if (C) pc++			*/
						stack[a] = (i >>> 23 != 0) ? LuaValue.TRUE : LuaValue.FALSE;
						if ((i & (0x1ff << 14)) != 0)
							++pc; /* skip next instruction (if C) */
						continue;

					case Lua.OP_LOADNIL: /*	A B	R(A):= ...:= R(A+B):= nil			*/
						for (b = i >>> 23; b-- >= 0; )
							stack[a++] = LuaValue.NIL;
						continue;

					case Lua.OP_GETUPVAL: /*	A B	R(A):= UpValue[B]				*/
						stack[a] = upValues[i >>> 23].getValue();
						continue;

					case Lua.OP_GETTABUP: /*	A B C	R(A) := UpValue[B][RK(C)]			*/
						stack[a] = upValues[i >>> 23].getValue().get((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_GETTABLE: /*	A B C	R(A):= R(B)[RK(C)]				*/
						stack[a] = stack[i >>> 23].get((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_SETTABUP: /*	A B C	UpValue[A][RK(B)] := RK(C)			*/
						upValues[a].getValue().set(((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]), (c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_SETUPVAL: /*	A B	UpValue[B]:= R(A)				*/
						upValues[i >>> 23].setValue(stack[a]);
						continue;

					case Lua.OP_SETTABLE: /*	A B C	R(A)[RK(B)]:= RK(C)				*/
						stack[a].set(((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]), (c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_NEWTABLE: /*	A B C	R(A):= {} (size = B,C)				*/
						stack[a] = new LuaTable(i >>> 23, (i >> 14) & 0x1ff);
						continue;

					case Lua.OP_SELF: /*	A B C	R(A+1):= R(B): R(A):= R(B)[RK(C)]		*/
						stack[a + 1] = (o = stack[i >>> 23]);
						stack[a] = o.get((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_ADD: /*	A B C	R(A):= RK(B) + RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).add((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_SUB: /*	A B C	R(A):= RK(B) - RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).sub((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_MUL: /*	A B C	R(A):= RK(B) * RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).mul((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_DIV: /*	A B C	R(A):= RK(B) / RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).div((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_MOD: /*	A B C	R(A):= RK(B) % RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).mod((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_POW: /*	A B C	R(A):= RK(B) ^ RK(C)				*/
						stack[a] = ((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).pow((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]);
						continue;

					case Lua.OP_UNM: /*	A B	R(A):= -R(B)					*/
						stack[a] = stack[i >>> 23].neg();
						continue;

					case Lua.OP_NOT: /*	A B	R(A):= not R(B)				*/
						stack[a] = stack[i >>> 23].not();
						continue;

					case Lua.OP_LEN: /*	A B	R(A):= length of R(B)				*/
						stack[a] = stack[i >>> 23].len();
						continue;

					case Lua.OP_CONCAT: /*	A B C	R(A):= R(B).. ... ..R(C)			*/
						b = i >>> 23;
						c = (i >> 14) & 0x1ff;
					{
						if (c > b + 1) {
							Buffer sb = stack[c].buffer();
							while (--c >= b)
								sb = stack[c].concat(sb);
							stack[a] = sb.value();
						} else {
							stack[a] = stack[c - 1].concat(stack[c]);
						}
					}
					continue;

					case Lua.OP_JMP: /*	sBx	pc+=sBx					*/
						pc += (i >>> 14) - 0x1ffff;
						if (a > 0) {
							for (--a, b = openups.length; --b >= 0; )
								if (openups[b] != null && getUpvalIndex(openups[b]) >= a) {
									openups[b].close();
									openups[b] = null;
								}
						}
						continue;

					case Lua.OP_EQ: /*	A B C	if ((RK(B) == RK(C)) ~= A) then pc++		*/
						if (((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).eq_b((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]) != (a != 0))
							++pc;
						continue;

					case Lua.OP_LT: /*	A B C	if ((RK(B) <  RK(C)) ~= A) then pc++  		*/
						if (((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).lt_b((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]) != (a != 0))
							++pc;
						continue;

					case Lua.OP_LE: /*	A B C	if ((RK(B) <= RK(C)) ~= A) then pc++  		*/
						if (((b = i >>> 23) > 0xff ? k[b & 0x0ff] : stack[b]).lteq_b((c = (i >> 14) & 0x1ff) > 0xff ? k[c & 0x0ff] : stack[c]) != (a != 0))
							++pc;
						continue;

					case Lua.OP_TEST: /*	A C	if not (R(A) <=> C) then pc++			*/
						if (stack[a].toboolean() != ((i & (0x1ff << 14)) != 0))
							++pc;
						continue;

					case Lua.OP_TESTSET: /*	A B C	if (R(B) <=> C) then R(A):= R(B) else pc++	*/
				/* note: doc appears to be reversed */
						if ((o = stack[i >>> 23]).toboolean() != ((i & (0x1ff << 14)) != 0))
							++pc;
						else
							stack[a] = o; // TODO: should be sBx?
						continue;

					case Lua.OP_CALL: /*	A B C	R(A), ... ,R(A+C-2):= R(A)(R(A+1), ... ,R(A+B-1)) */
						if (stack[a] instanceof ManagedLuaClosure) {
							nested = (ManagedLuaClosure) stack[a];
							nested.parent = this;
						}

						try {
							switch (i & (Lua.MASK_B | Lua.MASK_C)) {
								case (1 << Lua.POS_B) | (0 << Lua.POS_C):
									v = stack[a].invoke(NONE);
									top = a + v.narg();
									break;
								case (2 << Lua.POS_B) | (0 << Lua.POS_C):
									v = stack[a].invoke(stack[a + 1]);
									top = a + v.narg();
									break;
								case (1 << Lua.POS_B) | (1 << Lua.POS_C):

									stack[a].call();
									break;
								case (2 << Lua.POS_B) | (1 << Lua.POS_C):
									stack[a].call(stack[a + 1]);
									if (stack[a].isfunction()) {
										if (stack[a].checkfunction().name().equals("delay")) {
											numwait = stack[a + 1].checkint();
											parkReason = ParkReason.DELAY;
											pc++;
											luars.addPending(this);
											return NONE;
										}
									}
									break;
								case (3 << Lua.POS_B) | (1 << Lua.POS_C):
									stack[a].call(stack[a + 1], stack[a + 2]);
									break;
								case (4 << Lua.POS_B) | (1 << Lua.POS_C):
									stack[a].call(stack[a + 1], stack[a + 2], stack[a + 3]);
									break;
								case (1 << Lua.POS_B) | (2 << Lua.POS_C):
									stack[a] = stack[a].call();
									break;
								case (2 << Lua.POS_B) | (2 << Lua.POS_C):
									if (stack[a].isfunction()) {
										if (stack[a].checkfunction().name().equals("delay")) {
											numwait = stack[a + 1].checkint();
											pc++;
											return NONE;
										}
									}

									stack[a] = stack[a].call(stack[a + 1]);
									break;
								case (3 << Lua.POS_B) | (2 << Lua.POS_C):
									stack[a] = stack[a].call(stack[a + 1], stack[a + 2]);
									break;
								case (4 << Lua.POS_B) | (2 << Lua.POS_C):
									stack[a] = stack[a].call(stack[a + 1], stack[a + 2], stack[a + 3]);
									break;
								default:
									b = i >>> 23;
									c = (i >> 14) & 0x1ff;
									v = b > 0 ?
											varargsOf(stack, a + 1, b - 1) : // exact arg count
											varargsOf(stack, a + 1, top - v.narg() - (a + 1), v); // from prev top
									v = stack[a].invoke(v);
									if (c > 0) {
										while (--c > 0)
											stack[a + c - 1] = v.arg(c);
										v = NONE; // TODO: necessary?
									} else {
										top = a + v.narg();
									}
									break;
							}
						} finally {
							if (stack[a] instanceof ManagedLuaClosure) {
								pc++;
								return NONE;
							}
						}

						continue;

					case Lua.OP_TAILCALL: /*	A B C	return R(A)(R(A+1), ... ,R(A+B-1))		*/
						switch (i & Lua.MASK_B) {
							case (1 << Lua.POS_B):
								return new TailcallVarargs(stack[a], NONE);
							case (2 << Lua.POS_B):
								return new TailcallVarargs(stack[a], stack[a + 1]);
							case (3 << Lua.POS_B):
								return new TailcallVarargs(stack[a], varargsOf(stack[a + 1], stack[a + 2]));
							case (4 << Lua.POS_B):
								return new TailcallVarargs(stack[a], varargsOf(stack[a + 1], stack[a + 2], stack[a + 3]));
							default:
								b = i >>> 23;
								v = b > 0 ?
										varargsOf(stack, a + 1, b - 1) : // exact arg count
										varargsOf(stack, a + 1, top - v.narg() - (a + 1), v); // from prev top
								return new TailcallVarargs(stack[a], v);
						}

					case Lua.OP_RETURN: /*	A B	return R(A), ... ,R(A+B-2)	(see note)	*/
						b = i >>> 23;

						if (parent != null) {
							parent.nested = null;
						}

						try {
							switch (b) {
								case 0:
									return varargsOf(stack, a, top - v.narg() - a, v);
								case 1:
									return NONE;
								case 2:
									return stack[a];
								default:
									return varargsOf(stack, a, b - 1);
							}
						} finally {
							if (parent != null) {
								parent.execute(_stack, varargs);
							}
						}
					case Lua.OP_FORLOOP: /*	A sBx	R(A)+=R(A+2): if R(A) <?= R(A+1) then { pc+=sBx: R(A+3)=R(A) }*/ {
						LuaValue limit = stack[a + 1];
						LuaValue step = stack[a + 2];
						LuaValue idx = step.add(stack[a]);
						if (step.gt_b(0) ? idx.lteq_b(limit) : idx.gteq_b(limit)) {
							stack[a] = idx;
							stack[a + 3] = idx;
							pc += (i >>> 14) - 0x1ffff;
						}
					}
					continue;

					case Lua.OP_FORPREP: /*	A sBx	R(A)-=R(A+2): pc+=sBx				*/ {
						LuaValue init = stack[a].checknumber("'for' initial value must be a number");
						LuaValue limit = stack[a + 1].checknumber("'for' limit must be a number");
						LuaValue step = stack[a + 2].checknumber("'for' step must be a number");
						stack[a] = init.sub(step);
						stack[a + 1] = limit;
						stack[a + 2] = step;
						pc += (i >>> 14) - 0x1ffff;
					}
					continue;

					case Lua.OP_TFORCALL: /* A C	R(A+3), ... ,R(A+2+C) := R(A)(R(A+1), R(A+2));	*/
						v = stack[a].invoke(varargsOf(stack[a + 1], stack[a + 2]));
						c = (i >> 14) & 0x1ff;
						while (--c >= 0)
							stack[a + 3 + c] = v.arg(c + 1);
						v = NONE;
						continue;

					case Lua.OP_TFORLOOP: /* A sBx	if R(A+1) ~= nil then { R(A)=R(A+1); pc += sBx */
						if (!stack[a + 1].isnil()) { /* continue loop? */
							stack[a] = stack[a + 1];  /* save control varible. */
							pc += (i >>> 14) - 0x1ffff;
						}
						continue;

					case Lua.OP_SETLIST: /*	A B C	R(A)[(C-1)*FPF+i]:= R(A+i), 1 <= i <= B	*/ {
						if ((c = (i >> 14) & 0x1ff) == 0)
							c = code[++pc];
						int offset = (c - 1) * Lua.LFIELDS_PER_FLUSH;
						o = stack[a];
						if ((b = i >>> 23) == 0) {
							b = top - a - 1;
							int m = b - v.narg();
							int j = 1;
							for (; j <= m; j++)
								o.set(offset + j, stack[a + j]);
							for (; j <= b; j++)
								o.set(offset + j, v.arg(j - m));
						} else {
							o.presize(offset + b);
							for (int j = 1; j <= b; j++)
								o.set(offset + j, stack[a + j]);
						}
					}
					continue;

					case Lua.OP_CLOSURE: /*	A Bx	R(A):= closure(KPROTO[Bx])	*/ {
						Prototype newp = p.p[i >>> 14];
						ManagedLuaClosure ncl = new ManagedLuaClosure(luars, newp, null);
						Upvaldesc[] uv = newp.upvalues;
						for (int j = 0, nup = uv.length; j < nup; ++j) {
							if (uv[j].instack)  /* upvalue refes to local variable? */
								ncl.upValues[j] = findupval(stack, uv[j].idx, openups);
							else  /* get upvalue from enclosing function */
								ncl.upValues[j] = upValues[uv[j].idx];
						}
						stack[a] = ncl;
					}
					continue;

					case Lua.OP_VARARG: /*	A B	R(A), R(A+1), ..., R(A+B-1) = vararg		*/
						b = i >>> 23;
						if (b == 0) {
							top = a + (b = varargs.narg());
							v = varargs;
						} else {
							for (int j = 1; j < b; ++j)
								stack[a + j - 1] = varargs.arg(j);
						}
						continue;

					case Lua.OP_EXTRAARG:
						throw new IllegalArgumentException("Uexecutable opcode: OP_EXTRAARG");

					default:
						throw new IllegalArgumentException("Illegal opcode: " + (i & 0x3f));
				}
			}
		} catch (LuaError le) {
			throw le;
		} catch (Exception e) {
			throw new LuaError(e);
		} finally {
			if (openups != null)
				for (int u = openups.length; --u >= 0; )
					if (openups[u] != null)
						openups[u].close();
		}
	}
}
