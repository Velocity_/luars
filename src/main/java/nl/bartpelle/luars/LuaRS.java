package nl.bartpelle.luars;

import nl.bartpelle.luars.model.LuaPlayer;
import org.luaj.vm2.*;
import org.luaj.vm2.compiler.DumpState;
import org.luaj.vm2.compiler.LuaC;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.luaj.vm2.luajc.ProtoInfo;

import java.io.*;
import java.nio.file.Path;
import java.util.*;

/**
 * Created by Bart on 1/31/2015.
 *
 * Entry class for Lua RS functionality
 */
public class LuaRS {

	private List<Prototype> onLoginEvents = new LinkedList<>();
	private Map<Integer, List<Prototype>>[] buttonEvents = (Map<Integer, List<Prototype>>[]) new Map[10];
	private List<ManagedLuaClosure> pending = new LinkedList<>();
	private Globals globals = globals();
	private LuaImplementation implementation;

	public LuaRS(LuaImplementation implementation) {
		this.implementation = implementation;

		/* Create button event map */
		for (int i=0; i<10; i++)
			buttonEvents[i] = new HashMap<>();
	}

	public void processCycle() {
		List<ManagedLuaClosure> toRun = new LinkedList<>();

		for (Iterator<ManagedLuaClosure> i = pending.iterator(); i.hasNext(); ) {
			ManagedLuaClosure closure = i.next();

			if (closure.parkReason == ParkReason.DELAY) {
				if (--closure.numwait <= 0) {
					toRun.add(closure);
					i.remove();
					continue;
				}
			}

			/* Has it terminated? */
			if (closure.parkReason == null)
				i.remove();
		}

		toRun.forEach(ManagedLuaClosure::cont);
	}

	public void addPending(ManagedLuaClosure closure) {
		pending.add(closure);
	}

	public void loadRecursive(File dir) throws IOException {
		if (dir == null || !dir.exists())
			return;

		for (File f : dir.listFiles()) {
			try {
				if (f.isDirectory()) {
					loadRecursive(f);
				} else if (f.getName().endsWith("lcs")) {
					Prototype proto = loadCompiled(f);
					runPrototype(proto);
				} else if (f.getName().endsWith("lua")) {
					Prototype proto = loadRaw(f);
					runPrototype(proto);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public Prototype loadRaw(File script) throws IOException {
		return LuaC.instance.compile(new FileInputStream(script), script.getName());
	}

	public Prototype loadCompiled(File binary) throws IOException {
		String name = binary.getName();
		return LoadState.instance.undump(new FileInputStream(binary), name.substring(name.lastIndexOf('.')) + ".lua");
	}

	public byte[] precompile(File script) throws IOException {
		Prototype proto = loadRaw(script);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DumpState.dump(proto, baos, true);
		return baos.toByteArray();
	}

	public void runPrototype(Prototype proto, LuaValue... args) {
		ManagedLuaClosure f = new ManagedLuaClosure(this, proto, globals);
		f.run(args);
	}

	public void register(ListenerType type, Prototype proto) {
		switch (type) {
			case ON_LOGIN:
				onLoginEvents.add(proto);
				break;
		}
	}

	public void registerButton(ListenerType type, Prototype proto, int index, int hash) {
		if (!buttonEvents[index].containsKey(hash))
			buttonEvents[index].put(hash, new LinkedList<>());

		buttonEvents[index].get(hash).add(proto);
	}

	public void fire(ListenerType type, Object... args) {
		switch (type) {
			case ON_LOGIN: {
				LuaPlayer player = new LuaPlayer(implementation, args[0]);
				onLoginEvents.forEach((p) -> runPrototype(p, CoerceJavaToLua.coerce(player)));
				break;
			}
			case ON_BUTTON: {
				LuaPlayer player = new LuaPlayer(implementation, args[0]);
				int index = ((int) args[1]);
				int hash = args.length > 3 ? (((int) args[2]) << 16) | ((int) args[3]) : ((int) args[2]);
				if (buttonEvents[index].containsKey(hash))
					buttonEvents[index].get(hash).forEach((p) -> runPrototype(p, CoerceJavaToLua.coerce(player)));
				break;
			}
		}
	}

	private Globals globals() {
		Globals globals = JsePlatform.standardGlobals();
		globals.load(new LibRsps(this));
		return globals;
	}

	public static void main(String[] args) throws Exception {
		new LuaRS(null);
	}

}
