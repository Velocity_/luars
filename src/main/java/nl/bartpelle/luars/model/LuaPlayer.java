package nl.bartpelle.luars.model;

import nl.bartpelle.luars.LuaImplementation;

/**
 * Created by Bart on 2/1/2015.
 *
 * A wrapper for the player to provide portability.
 */
public class LuaPlayer {

	private Object reference;
	private LuaImplementation implementation;

	/* Fields for the script to access, set upon creation */
	public String name;

	public LuaPlayer(LuaImplementation implementation, Object reference) {
		this.reference = reference;
		this.implementation = implementation;

		this.name = implementation.playerName(reference);
	}

	public void animate(int id) {
		implementation.animatePlayer(reference, id);
	}

	public void message(String msg) {
		implementation.messagePlayer(reference, msg);
	}

	public void open_interface(int id, int target, int targetSub, boolean walkable) {
		implementation.openInterface(reference, id, target, targetSub, walkable);
	}

}
